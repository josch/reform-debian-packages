From bf2d0fcd4a7ebd5dd713aa04efd183bffaabf72b Mon Sep 17 00:00:00 2001
From: Cristian Ciocaltea <cristian.ciocaltea@collabora.com>
Date: Thu, 28 Mar 2024 00:47:03 +0200
Subject: [PATCH 48/68] dt-bindings: display: rockchip,dw-hdmi: Add compatible
 for RK3588

Document the DW HDMI TX Controller found on Rockchip RK3588 SoC.

Since RK3588 uses different clocks than previous Rockchip SoCs and also
requires a couple of reset lines and some additional properties, provide
the required changes in the binding to be able to handle all variants.

Signed-off-by: Cristian Ciocaltea <cristian.ciocaltea@collabora.com>
---
 .../display/rockchip/rockchip,dw-hdmi.yaml    | 124 +++++++++++++-----
 1 file changed, 89 insertions(+), 35 deletions(-)

diff --git a/Documentation/devicetree/bindings/display/rockchip/rockchip,dw-hdmi.yaml b/Documentation/devicetree/bindings/display/rockchip/rockchip,dw-hdmi.yaml
index 2aac62219ff6..651189de7af5 100644
--- a/Documentation/devicetree/bindings/display/rockchip/rockchip,dw-hdmi.yaml
+++ b/Documentation/devicetree/bindings/display/rockchip/rockchip,dw-hdmi.yaml
@@ -12,10 +12,8 @@ maintainers:
 description: |
   The HDMI transmitter is a Synopsys DesignWare HDMI 1.4 TX controller IP
   with a companion PHY IP.
-
-allOf:
-  - $ref: ../bridge/synopsys,dw-hdmi.yaml#
-  - $ref: /schemas/sound/dai-common.yaml#
+  RK3588 SoC updated the TX controller IP to DW HDMI 2.1 Quad-Pixel (QP),
+  while making use of a HDMI/eDP TX Combo PHY based on a Samsung IP block.
 
 properties:
   compatible:
@@ -25,6 +23,7 @@ properties:
       - rockchip,rk3328-dw-hdmi
       - rockchip,rk3399-dw-hdmi
       - rockchip,rk3568-dw-hdmi
+      - rockchip,rk3588-dw-hdmi
 
   reg-io-width:
     const: 4
@@ -40,36 +39,6 @@ properties:
       A 1.8V supply that powers up the SoC internal circuitry. The pin name on the
       SoC usually is HDMI_TX_AVDD_1V8.
 
-  clocks:
-    minItems: 2
-    items:
-      - {}
-      - {}
-      # The next three clocks are all optional, but shall be specified in this
-      # order when present.
-      - description: The HDMI CEC controller main clock
-      - description: Power for GRF IO
-      - description: External clock for some HDMI PHY (old clock name, deprecated)
-      - description: External clock for some HDMI PHY (new name)
-
-  clock-names:
-    minItems: 2
-    items:
-      - {}
-      - {}
-      - enum:
-          - cec
-          - grf
-          - vpll
-          - ref
-      - enum:
-          - grf
-          - vpll
-          - ref
-      - enum:
-          - vpll
-          - ref
-
   ddc-i2c-bus:
     $ref: /schemas/types.yaml#/definitions/phandle
     description:
@@ -131,13 +100,98 @@ properties:
 required:
   - compatible
   - reg
-  - reg-io-width
   - clocks
   - clock-names
   - interrupts
   - ports
   - rockchip,grf
 
+allOf:
+  - $ref: /schemas/sound/dai-common.yaml#
+  - if:
+      properties:
+        compatible:
+          contains:
+            enum:
+              - rockchip,rk3588-dw-hdmi
+    then:
+      properties:
+        reg:
+          maxItems: 1
+
+        clocks:
+          minItems: 1
+          items:
+            - description: APB system interface clock
+            # The next clocks are optional, but shall be specified in this
+            # order when present.
+            - description: TMDS/FRL link clock
+            - description: EARC RX biphase clock
+            - description: Reference clock
+            - description: Audio interface clock
+            - description: Video datapath clock
+
+        clock-names:
+          minItems: 1
+          items:
+            - const: pclk
+            - enum: [hdp, earc, ref, aud, hclk_vo1]
+            - enum: [earc, ref, aud, hclk_vo1]
+            - enum: [ref, aud, hclk_vo1]
+            - enum: [aud, hclk_vo1]
+            - const: hclk_vo1
+
+        resets:
+          minItems: 2
+          maxItems: 2
+
+        reset-names:
+          items:
+            - const: ref
+            - const: hdp
+
+        interrupts:
+          minItems: 1
+          maxItems: 5
+
+        rockchip,vo1_grf:
+          $ref: /schemas/types.yaml#/definitions/phandle
+          description:
+            phandle to the VO1 GRF
+
+      required:
+        - resets
+        - reset-names
+        - rockchip,vo1_grf
+
+    else:
+      $ref: ../bridge/synopsys,dw-hdmi.yaml#
+
+      properties:
+        clocks:
+          minItems: 2
+          items:
+            - {}
+            - {}
+            # The next three clocks are all optional, but shall be specified in this
+            # order when present.
+            - description: The HDMI CEC controller main clock
+            - description: Power for GRF IO
+            - description: External clock for some HDMI PHY (old clock name, deprecated)
+            - description: External clock for some HDMI PHY (new name)
+
+        clock-names:
+          minItems: 2
+          items:
+            - {}
+            - {}
+            - enum: [cec, grf, vpll, ref]
+            - enum: [grf, vpll, ref]
+            - enum: [vpll, ref]
+
+      required:
+        - reg-io-width
+
 unevaluatedProperties: false
 
 examples:
-- 
2.39.2

