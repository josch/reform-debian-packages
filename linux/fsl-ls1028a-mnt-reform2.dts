// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Device Tree file for MNT Reform 2 with RBZ LS1028A SOM.
 *
 * Copyright 2022 MNT Research GmbH
 *
 * Lukas F. Hartmann <lukas@mntre.com>
 *
 */

/dts-v1/;
#include "fsl-ls1028a.dtsi"

#include <dt-bindings/gpio/gpio.h>

/ {
	model = "MNT Reform 2 with LS1028A Module";
	compatible = "fsl,ls1028a-rdb", "fsl,ls1028a";

	aliases {
		crypto = &crypto;
		serial0 = &duart0;
		serial1 = &duart1;
		serial2 = &lpuart1;
		mmc0 = &esdhc;
		mmc1 = &esdhc1;
		rtc1 = &ftm_alarm0;
	};

	chosen {
		stdout-path = "serial0:115200n8";
	};

	memory@80000000 {
		device_type = "memory";
		reg = <0x0 0x80000000 0x1 0x0000000>;
	};

	sys_mclk: clock-mclk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <12000000>;
	};

	reg_1p8v: regulator-1p8v {
		compatible = "regulator-fixed";
		regulator-name = "1P8V";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		regulator-always-on;
	};

	sb_3v3: regulator-sb3v3 {
		compatible = "regulator-fixed";
		regulator-name = "3v3_vbus";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-boot-on;
		regulator-always-on;
	};

	sound {
		compatible = "fsl,imx-audio-wm8960";
		model = "wm8960-audio";
		audio-cpu = <&sai4>;
		audio-codec = <&wm8960>;

		audio-routing =
			"Headphone Jack", "HP_L",
			"Headphone Jack", "HP_R",
			"Ext Spk", "SPK_LP",
			"Ext Spk", "SPK_LN",
			"Ext Spk", "SPK_RP",
			"Ext Spk", "SPK_RN",
			"LINPUT1", "Mic Jack",
			"Mic Jack", "MICB",
			"LINPUT2", "Line In Jack",
			"RINPUT2", "Line In Jack";
	};

	backlight: backlight {
		compatible = "pwm-backlight";
		pwms = <&pwm0 1 10000 0>;
		brightness-levels = <0 8 16 32 64 128 160 200 255>;
		default-brightness-level = <8>;
		enable-gpios = <&gpio1 24 GPIO_ACTIVE_HIGH>;
	};
};

&dpclk {
	// DP VCO clock is eDP pixel clock (162MHz) divided by 1.25
	fsl,vco-hz = <1296000000>;
};

&duart0 {
	status = "okay";
};

&duart1 {
	status = "okay";
};

// connected to LPC11U24 chip on the motherboard
&lpuart1 {
	status = "okay";
};

&esdhc {
	sd-uhs-sdr50;
	sd-uhs-sdr25;
	sd-uhs-sdr12;
	bus-width = <1>;
	status = "okay";
};

&esdhc1 {
	mmc-hs200-1_8v;
	bus-width = <8>;
	status = "okay";
};

&fspi {
	status = "disabled";
};

&i2c0 {
	status = "okay";

	// Audio chip on motherboard
	wm8960: codec@1a {
		#sound-dai-cells = <0>;
		compatible = "wlf,wm8960";
		reg = <0x1a>;
		clocks = <&sys_mclk>;
		clock-names = "mclk";
	};

	// Realtime clock chip on motherboard
	pcf8523: pcf8523@68 {
		compatible = "nxp,pcf8523";
		reg = <0x68>;
	};
};

&enetc_port0 {
	phy-handle = <&sgmii_phy0>;
	phy-connection-type = "sgmii";
	managed = "in-band-status";
	status = "okay";
};

&enetc_mdio_pf3 {
	sgmii_phy0: ethernet-phy@0 {
		reg = <0x0>;
	};
};

/*
XSPI1_A_DATA4 = GPIO2_DAT28
XSPI1_A_DATA5 = GPIO2_DAT29
XSPI1_A_DATA6 = GPIO2_DAT30
*/

&pcie1 {
	status = "okay";
};

&pcie2 {
	status = "okay";
};

&pwm0 {
	status = "okay";
};

&sata {
	status = "okay";
	dma-coherent;
};

&sai4 {
	fsl,sai-mclk-direction-output;
	status = "okay";
};

&usb0 {
	dr_mode = "host";
	status = "okay";
};

&usb1 {
	dr_mode = "host";
	status = "okay";
};

&dspi2 {
	status = "okay";
	spidev@0 {
		compatible = "mntre,lpc11u24";
		spi-max-frequency = <1000000>;
		reg = <0>;
	};
};
