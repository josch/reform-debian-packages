// SPDX-License-Identifier: (GPL-2.0+ OR MIT)

/*
 * Copyright 2019-2022 MNT Research GmbH
 * Copyright 2021 Lucas Stach <dev@lynxeye.de>
 * Copyright 2020 Boundary Devices
*/

// https://github.com/boundarydevices/linux/blob/boundary-imx_5.15.y/arch/arm64/boot/dts/freescale/imx8mp-nitrogen8mp.dts

/dts-v1/;

#include <dt-bindings/phy/phy-imx8-pcie.h>
#include "imx8mp.dtsi"

&iomuxc {
	pinctrl_eqos: eqosgrp {
		fsl,pins = <
			MX8MP_IOMUXC_ENET_MDC__ENET_QOS_MDC		0x20
			MX8MP_IOMUXC_ENET_MDIO__ENET_QOS_MDIO		0xa0
			MX8MP_IOMUXC_ENET_TX_CTL__ENET_QOS_RGMII_TX_CTL	0x1f
			MX8MP_IOMUXC_ENET_TXC__CCM_ENET_QOS_CLOCK_GENERATE_TX_CLK	0x1f
			MX8MP_IOMUXC_ENET_TD0__ENET_QOS_RGMII_TD0		0x1f
			MX8MP_IOMUXC_ENET_TD1__ENET_QOS_RGMII_TD1		0x1f
			MX8MP_IOMUXC_ENET_TD2__ENET_QOS_RGMII_TD2		0x1f
			MX8MP_IOMUXC_ENET_TD3__ENET_QOS_RGMII_TD3		0x1f

			MX8MP_IOMUXC_ENET_RX_CTL__ENET_QOS_RGMII_RX_CTL	0x91
			MX8MP_IOMUXC_ENET_RXC__CCM_ENET_QOS_CLOCK_GENERATE_RX_CLK	0x91
			MX8MP_IOMUXC_ENET_RD0__ENET_QOS_RGMII_RD0	0x91
			MX8MP_IOMUXC_ENET_RD1__ENET_QOS_RGMII_RD1	0x91
			MX8MP_IOMUXC_ENET_RD2__ENET_QOS_RGMII_RD2	0x91
			MX8MP_IOMUXC_ENET_RD3__ENET_QOS_RGMII_RD3	0x91
#define GP_EQOS_RESET	<&gpio3 16 GPIO_ACTIVE_LOW>
			MX8MP_IOMUXC_NAND_READY_B__GPIO3_IO16		0x100
#define GPIRQ_EQOS_PHY	<&gpio3 14 IRQ_TYPE_LEVEL_LOW>
			MX8MP_IOMUXC_NAND_DQS__GPIO3_IO14 0x10
		>;
	};

	pinctrl_hog: hoggrp {
		fsl,pins = <
			//MX8MP_IOMUXC_NAND_DATA01__GPIO3_IO07		0x119			/* J31 */
			MX8MP_IOMUXC_SPDIF_RX__GPIO5_IO04		0x41	/* Pin 17 */
			MX8MP_IOMUXC_SPDIF_EXT_CLK__GPIO5_IO05		0x41	/* Pin 19 */
			MX8MP_IOMUXC_SPDIF_TX__GPIO5_IO03		0x41	/* Pin 21 */
			MX8MP_IOMUXC_SD1_RESET_B__GPIO2_IO10		0x41	/* Pin 23 */
			MX8MP_IOMUXC_SD1_DATA6__GPIO2_IO08		0x41	/* Pin 25 */
			MX8MP_IOMUXC_SD1_DATA5__GPIO2_IO07		0x41	/* Pin 29 */
			//MX8MP_IOMUXC_SD1_DATA4__GPIO2_IO06		0x41	/* Pin 31 */
			MX8MP_IOMUXC_SAI1_MCLK__GPIO4_IO20		0x16	/* Pin 2 */
			MX8MP_IOMUXC_SAI1_TXD7__GPIO4_IO19		0x1c4	/* Pin 4 */
		>;
	};

	pinctrl_panel: panelgrp {
		fsl,pins = <
			MX8MP_IOMUXC_GPIO1_IO06__GPIO1_IO06	0x140
			MX8MP_IOMUXC_GPIO1_IO07__GPIO1_IO07	0x140
			MX8MP_IOMUXC_SD1_DATA4__GPIO2_IO06		0x41	/* Pin 31 */
		>;
	};

	pinctrl_hdmi: hdmigrp {
		fsl,pins = <
			MX8MP_IOMUXC_HDMI_DDC_SCL__HDMIMIX_HDMI_SCL	0x1c3
			MX8MP_IOMUXC_HDMI_DDC_SDA__HDMIMIX_HDMI_SDA	0x1c3
			MX8MP_IOMUXC_HDMI_HPD__HDMIMIX_HDMI_HPD		0x19
			MX8MP_IOMUXC_HDMI_CEC__HDMIMIX_HDMI_CEC		0x19

			/*MX8MP_IOMUXC_HDMI_DDC_SCL__HDMIMIX_HDMI_SCL	0x400001c3
			MX8MP_IOMUXC_HDMI_DDC_SDA__HDMIMIX_HDMI_SDA	0x400001c3
			MX8MP_IOMUXC_HDMI_HPD__HDMIMIX_HDMI_HPD		0x40000019
			MX8MP_IOMUXC_HDMI_CEC__HDMIMIX_HDMI_CEC		0x40000019*/
		>;
	};

	pinctrl_bt_rfkill: bt-rfkillgrp {
		fsl,pins = <
#define GP_BT_RFKILL_RESET	<&gpio3 9 GPIO_ACTIVE_LOW>
			MX8MP_IOMUXC_NAND_DATA03__GPIO3_IO09	0x119
		>;
	};

	pinctrl_ecspi2: ecspi2grp {
		fsl,pins = <
			/* J31 */
			MX8MP_IOMUXC_ECSPI2_SCLK__ECSPI2_SCLK		0x82	/* Pin 20 */
			MX8MP_IOMUXC_ECSPI2_MOSI__ECSPI2_MOSI		0x82	/* Pin 10 */
			MX8MP_IOMUXC_ECSPI2_MISO__ECSPI2_MISO		0x82	/* Pin 8 */
#define GP_ECSPI2_CS	<&gpio5 13 GPIO_ACTIVE_LOW>
			MX8MP_IOMUXC_ECSPI2_SS0__GPIO5_IO13		0x143	/* Pin 6 */
		>;
	};

	pinctrl_i2c1: i2c1grp {
		fsl,pins = <
			MX8MP_IOMUXC_I2C1_SCL__I2C1_SCL		0x400001c3
			MX8MP_IOMUXC_I2C1_SDA__I2C1_SDA		0x400001c3
		>;
	};

	pinctrl_i2c1_1: i2c1-1grp {
		fsl,pins = <
#define GP_I2C1_SCL	<&gpio5 14 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C1_SCL__GPIO5_IO14					0x1c3
#define GP_I2C1_SDA	<&gpio5 15 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C1_SDA__GPIO5_IO15					0x1c3
		>;
	};

	pinctrl_i2c2: i2c2grp {
		fsl,pins = <
			MX8MP_IOMUXC_I2C2_SCL__I2C2_SCL			0x400001c3
			MX8MP_IOMUXC_I2C2_SDA__I2C2_SDA			0x400001c3
		>;
	};

	pinctrl_i2c2_1: i2c2-1grp {
		fsl,pins = <
#define GP_I2C2_SCL	<&gpio5 16 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C2_SCL__GPIO5_IO16					0x1c3
#define GP_I2C2_SDA	<&gpio5 17 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C2_SDA__GPIO5_IO17					0x1c3
		>;
	};

	pinctrl_i2c3: i2c3grp {
		fsl,pins = <
			MX8MP_IOMUXC_I2C3_SCL__I2C3_SCL			0x400001c3
			MX8MP_IOMUXC_I2C3_SDA__I2C3_SDA			0x400001c3
		>;
	};

	pinctrl_i2c3_1: i2c3-1grp {
		fsl,pins = <
#define GP_I2C3_SCL	<&gpio5 18 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C3_SCL__GPIO5_IO18					0x1c3
#define GP_I2C3_SDA	<&gpio5 19 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C3_SDA__GPIO5_IO19					0x1c3
		>;
	};

	pinctrl_i2c4: i2c4grp {
		fsl,pins = <
			MX8MP_IOMUXC_I2C4_SCL__I2C4_SCL			0x400001c3
			MX8MP_IOMUXC_I2C4_SDA__I2C4_SDA			0x400001c3
		>;
	};

	pinctrl_i2c4_1: i2c4-1grp {
		fsl,pins = <
#define GP_I2C4_SCL	<&gpio5 20 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C4_SCL__GPIO5_IO20					0x1c3
#define GP_I2C4_SDA	<&gpio5 21 GPIO_OPEN_DRAIN>
			MX8MP_IOMUXC_I2C4_SDA__GPIO5_IO21					0x1c3
		>;
	};

	pinctrl_reg_wlan_vmmc: reg-wlan-vmmcgrp {
		fsl,pins = <
#define GP_REG_WLAN_VMMC	<&gpio2 19 GPIO_ACTIVE_HIGH>
			MX8MP_IOMUXC_SD2_RESET_B__GPIO2_IO19	0x16
		>;
	};

	pinctrl_sai2: sai2grp {
		fsl,pins = <
			MX8MP_IOMUXC_SAI2_TXFS__AUDIOMIX_SAI2_TX_SYNC	0xd6
			MX8MP_IOMUXC_SAI2_RXFS__AUDIOMIX_SAI2_RX_SYNC	0xd6
			MX8MP_IOMUXC_SAI2_TXC__AUDIOMIX_SAI2_TX_BCLK	0xd6
			MX8MP_IOMUXC_SAI2_RXC__AUDIOMIX_SAI2_RX_BCLK	0xd6
			MX8MP_IOMUXC_SAI2_RXD0__AUDIOMIX_SAI2_RX_DATA00	0xd6
			MX8MP_IOMUXC_SAI2_TXD0__AUDIOMIX_SAI2_TX_DATA00	0xd6
			MX8MP_IOMUXC_SAI2_MCLK__AUDIOMIX_SAI2_MCLK	0xd6
		>;
	};

	pinctrl_sai3: sai3grp {
		fsl,pins = <
			MX8MP_IOMUXC_SAI3_TXFS__AUDIOMIX_SAI3_TX_SYNC	0xd6
			MX8MP_IOMUXC_SAI3_RXFS__AUDIOMIX_SAI3_RX_SYNC	0xd6
			MX8MP_IOMUXC_SAI3_TXC__AUDIOMIX_SAI3_TX_BCLK	0xd6
			MX8MP_IOMUXC_SAI3_RXC__AUDIOMIX_SAI3_RX_BCLK	0xd6
			MX8MP_IOMUXC_SAI3_RXD__AUDIOMIX_SAI3_RX_DATA00	0xd6
			MX8MP_IOMUXC_SAI3_TXD__AUDIOMIX_SAI3_TX_DATA00	0xd6
			MX8MP_IOMUXC_SAI3_MCLK__AUDIOMIX_SAI3_MCLK	0xd6
		>;
	};

	pinctrl_pcie: pciegrp {
		fsl,pins = <
#define GP_PCIE_RESET	<&gpio1 11 GPIO_ACTIVE_LOW>
			MX8MP_IOMUXC_GPIO1_IO11__GPIO1_IO11	0x41
/*#define GP_PCIE_DISABLE	<&gpio4 26 GPIO_ACTIVE_LOW>
			MX8MP_IOMUXC_SAI2_TXD0__GPIO4_IO26	0x41*/
		>;
	};

	pinctrl_pmic: pmicirq {
		fsl,pins = <
#define GPIRQ_PMIC	<&gpio3 0 IRQ_TYPE_LEVEL_LOW>
			MX8MP_IOMUXC_NAND_ALE__GPIO3_IO00	0x41
		>;
	};

	pinctrl_edp_bridge: edpbridgegrp {
		fsl,pins = <
			MX8MP_IOMUXC_GPIO1_IO04__GPIO1_IO04 0x100
		>;
	};

	pinctrl_uart1: uart1grp {
		fsl,pins = <
			MX8MP_IOMUXC_UART1_RXD__UART1_DCE_RX	0x140
			MX8MP_IOMUXC_UART1_TXD__UART1_DCE_TX	0x140
			MX8MP_IOMUXC_UART3_RXD__UART1_DCE_CTS	0x140
			MX8MP_IOMUXC_UART3_TXD__UART1_DCE_RTS	0x140
#define GP_BT_ENABLE	<&gpio3 9 GPIO_ACTIVE_HIGH>
			MX8MP_IOMUXC_NAND_DATA03__GPIO3_IO09	0x119
		>;
	};

	pinctrl_uart2: uart2grp {
		fsl,pins = <
			MX8MP_IOMUXC_UART2_RXD__UART2_DCE_RX	0x140
			MX8MP_IOMUXC_UART2_TXD__UART2_DCE_TX	0x140
		>;
	};

	pinctrl_uart3: uart3grp {
		fsl,pins = <
			MX8MP_IOMUXC_ECSPI1_SCLK__UART3_DCE_RX	0x140
			MX8MP_IOMUXC_ECSPI1_MOSI__UART3_DCE_TX	0x140
		>;
	};

	pinctrl_uart4: uart4grp {
		fsl,pins = <
			MX8MP_IOMUXC_UART4_RXD__UART4_DCE_RX	0x140
			MX8MP_IOMUXC_UART4_TXD__UART4_DCE_TX	0x140
		>;
	};

	pinctrl_usb1_vbus: usb1grp {
		fsl,pins = <
			MX8MP_IOMUXC_GPIO1_IO14__USB2_OTG_PWR	0x10
		>;
	};

	pinctrl_usdhc1: usdhc1grp {
		fsl,pins = <
			MX8MP_IOMUXC_SD1_CLK__USDHC1_CLK	0x190
			MX8MP_IOMUXC_SD1_CMD__USDHC1_CMD	0x1d0
			MX8MP_IOMUXC_SD1_DATA0__USDHC1_DATA0	0x1d0
			MX8MP_IOMUXC_SD1_DATA1__USDHC1_DATA1	0x1d0
			MX8MP_IOMUXC_SD1_DATA2__USDHC1_DATA2	0x1d0
			MX8MP_IOMUXC_SD1_DATA3__USDHC1_DATA3	0x1d0
			//MX8MP_IOMUXC_GPIO1_IO03__USDHC1_VSELECT	0x116
		>;
	};

	pinctrl_usdhc1_100mhz: usdhc1grp-100mhz {
		fsl,pins = <
			MX8MP_IOMUXC_SD1_CLK__USDHC1_CLK	0x194
			MX8MP_IOMUXC_SD1_CMD__USDHC1_CMD	0x1d4
			MX8MP_IOMUXC_SD1_DATA0__USDHC1_DATA0	0x1d4
			MX8MP_IOMUXC_SD1_DATA1__USDHC1_DATA1	0x1d4
			MX8MP_IOMUXC_SD1_DATA2__USDHC1_DATA2	0x1d4
			MX8MP_IOMUXC_SD1_DATA3__USDHC1_DATA3	0x1d4
		>;
	};

	pinctrl_usdhc1_200mhz: usdhc1grp-200mhz {
		fsl,pins = <
			MX8MP_IOMUXC_SD1_CLK__USDHC1_CLK	0x196
			MX8MP_IOMUXC_SD1_CMD__USDHC1_CMD	0x1d6
			MX8MP_IOMUXC_SD1_DATA0__USDHC1_DATA0	0x1d6
			MX8MP_IOMUXC_SD1_DATA1__USDHC1_DATA1	0x1d6
			MX8MP_IOMUXC_SD1_DATA2__USDHC1_DATA2	0x1d6
			MX8MP_IOMUXC_SD1_DATA3__USDHC1_DATA3	0x1d6
		>;
	};

	pinctrl_usdhc1_gpio: usdhc1grp-gpio {
		fsl,pins = <
#define GP_USDHC1_CD	<&gpio2 11 GPIO_ACTIVE_LOW>
			MX8MP_IOMUXC_SD1_STROBE__GPIO2_IO11	0x1c4
		>;
	};

	pinctrl_usdhc2: usdhc2grp {
		fsl,pins = <
			MX8MP_IOMUXC_SD2_CLK__USDHC2_CLK	0x190
			MX8MP_IOMUXC_SD2_CMD__USDHC2_CMD	0x1d0
			MX8MP_IOMUXC_SD2_DATA0__USDHC2_DATA0	0x1d0
			MX8MP_IOMUXC_SD2_DATA1__USDHC2_DATA1	0x1d0
			MX8MP_IOMUXC_SD2_DATA2__USDHC2_DATA2	0x1d0
			MX8MP_IOMUXC_SD2_DATA3__USDHC2_DATA3	0x1d0
		>;
	};

	pinctrl_usdhc2_100mhz: usdhc2grp100mhz {
		fsl,pins = <
			MX8MP_IOMUXC_SD2_CLK__USDHC2_CLK	0x194
			MX8MP_IOMUXC_SD2_CMD__USDHC2_CMD	0x1d4
			MX8MP_IOMUXC_SD2_DATA0__USDHC2_DATA0	0x1d4
			MX8MP_IOMUXC_SD2_DATA1__USDHC2_DATA1	0x1d4
			MX8MP_IOMUXC_SD2_DATA2__USDHC2_DATA2	0x1d4
			MX8MP_IOMUXC_SD2_DATA3__USDHC2_DATA3	0x1d4
		>;
	};

	pinctrl_usdhc2_200mhz: usdhc2grp200mhz {
		fsl,pins = <
			MX8MP_IOMUXC_SD2_CLK__USDHC2_CLK	0x196
			MX8MP_IOMUXC_SD2_CMD__USDHC2_CMD	0x1d6
			MX8MP_IOMUXC_SD2_DATA0__USDHC2_DATA0	0x1d6
			MX8MP_IOMUXC_SD2_DATA1__USDHC2_DATA1	0x1d6
			MX8MP_IOMUXC_SD2_DATA2__USDHC2_DATA2	0x1d6
			MX8MP_IOMUXC_SD2_DATA3__USDHC2_DATA3	0x1d6
		>;
	};

	pinctrl_usdhc3: usdhc3grp {
		fsl,pins = <
			MX8MP_IOMUXC_NAND_WE_B__USDHC3_CLK	0x190
			MX8MP_IOMUXC_NAND_WP_B__USDHC3_CMD	0x1d0
			MX8MP_IOMUXC_NAND_DATA04__USDHC3_DATA0	0x1d0
			MX8MP_IOMUXC_NAND_DATA05__USDHC3_DATA1	0x1d0
			MX8MP_IOMUXC_NAND_DATA06__USDHC3_DATA2	0x1d0
			MX8MP_IOMUXC_NAND_DATA07__USDHC3_DATA3	0x1d0
			MX8MP_IOMUXC_NAND_RE_B__USDHC3_DATA4	0x1d0
			MX8MP_IOMUXC_NAND_CE2_B__USDHC3_DATA5	0x1d0
			MX8MP_IOMUXC_NAND_CE3_B__USDHC3_DATA6	0x1d0
			MX8MP_IOMUXC_NAND_CLE__USDHC3_DATA7	0x1d0
			MX8MP_IOMUXC_NAND_CE1_B__USDHC3_STROBE	0x10
#define GP_EMMC_RESET	<&gpio3 1 IRQ_TYPE_LEVEL_LOW>
			MX8MP_IOMUXC_NAND_CE0_B__GPIO3_IO01	0x140
		>;
	};

	pinctrl_usdhc3_100mhz: usdhc3grp-100mhz {
		fsl,pins = <
			MX8MP_IOMUXC_NAND_WE_B__USDHC3_CLK	0x194
			MX8MP_IOMUXC_NAND_WP_B__USDHC3_CMD	0x1d4
			MX8MP_IOMUXC_NAND_DATA04__USDHC3_DATA0	0x1d4
			MX8MP_IOMUXC_NAND_DATA05__USDHC3_DATA1	0x1d4
			MX8MP_IOMUXC_NAND_DATA06__USDHC3_DATA2	0x1d4
			MX8MP_IOMUXC_NAND_DATA07__USDHC3_DATA3	0x1d4
			MX8MP_IOMUXC_NAND_RE_B__USDHC3_DATA4	0x1d4
			MX8MP_IOMUXC_NAND_CE2_B__USDHC3_DATA5	0x1d4
			MX8MP_IOMUXC_NAND_CE3_B__USDHC3_DATA6	0x1d4
			MX8MP_IOMUXC_NAND_CLE__USDHC3_DATA7	0x1d4
			MX8MP_IOMUXC_NAND_CE1_B__USDHC3_STROBE	0x14
		>;
	};

	pinctrl_usdhc3_200mhz: usdhc3grp-200mhz {
		fsl,pins = <
			MX8MP_IOMUXC_NAND_WE_B__USDHC3_CLK	0x196
			MX8MP_IOMUXC_NAND_WP_B__USDHC3_CMD	0x1d6
			MX8MP_IOMUXC_NAND_DATA04__USDHC3_DATA0	0x1d6
			MX8MP_IOMUXC_NAND_DATA05__USDHC3_DATA1	0x1d6
			MX8MP_IOMUXC_NAND_DATA06__USDHC3_DATA2	0x1d6
			MX8MP_IOMUXC_NAND_DATA07__USDHC3_DATA3	0x1d6
			MX8MP_IOMUXC_NAND_RE_B__USDHC3_DATA4	0x1d6
			MX8MP_IOMUXC_NAND_CE2_B__USDHC3_DATA5	0x1d6
			MX8MP_IOMUXC_NAND_CE3_B__USDHC3_DATA6	0x1d6
			MX8MP_IOMUXC_NAND_CLE__USDHC3_DATA7	0x1d6
			MX8MP_IOMUXC_NAND_CE1_B__USDHC3_STROBE	0x12
		>;
	};

	pinctrl_wdog: wdoggrp {
		fsl,pins = <
			MX8MP_IOMUXC_GPIO1_IO02__WDOG1_WDOG_B	0xc6
		>;
	};
};

/ {
	model = "MNT Pocket Reform with i.MX8MP Module";
	compatible = "mntre,pocket-reform", "boundary,imx8mp-nitrogen8mp-som", "fsl,imx8mp";
	chassis-type = "laptop";

	chosen {
		stdout-path = &uart2;
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0x0 0x40000000 0 0x80000000>;
	};

	bt-rfkill {
		compatible = "net,rfkill-gpio";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_bt_rfkill>;
		name = "bt-rfkill";
		type = <2>; /* Bluetooth */
		reset-gpios = GP_BT_RFKILL_RESET;
		status = "okay";
	};

	pcie0_refclk: pcie0-refclk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <100000000>;
	};

	reg_main_5v: regulator-main-5v {
		compatible = "regulator-fixed";
		regulator-name = "5V";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
	};

	reg_main_3v3: regulator-main-3v3 {
		compatible = "regulator-fixed";
		regulator-name = "3V3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	reg_main_usb: regulator-main-usb {
		compatible = "regulator-fixed";
		regulator-name = "USB_PWR";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		vin-supply = <&reg_main_5v>;
	};

	reg_main_1v8: regulator-main-1v8 {
		compatible = "regulator-fixed";
		regulator-name = "1V8";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&reg_main_3v3>;
	};

	reg_main_1v2: regulator-main-1v2 {
		compatible = "regulator-fixed";
		regulator-name = "1V2";
		regulator-min-microvolt = <1200000>;
		regulator-max-microvolt = <1200000>;
		vin-supply = <&reg_main_5v>;
	};

	reg_wlan_vmmc: regulator-wlan-vmmc {
		compatible = "regulator-fixed";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_reg_wlan_vmmc>;
		regulator-name = "reg_wlan_vmmc";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = GP_REG_WLAN_VMMC;
		startup-delay-us = <70000>;
		enable-active-high;
	};

	reg_pcie0: regulator-pcie {
		compatible = "regulator-fixed";
		regulator-name = "MPCIE_3V3";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		vin-supply = <&reg_main_5v>;
	};

	sound {
		compatible = "simple-audio-card";
		status = "okay";
		simple-audio-card,name = "tlv320aic3100";
		simple-audio-card,format = "i2s";
		simple-audio-card,frame-master = <&codec_dai>;
		simple-audio-card,bitclock-master = <&codec_dai>;

		simple-audio-card,widgets =
			"Microphone", "Microphone Jack",
			"Headphone", "Headphone Jack",
			"Speaker", "Speaker";

		simple-audio-card,routing =
			"MIC1RP", "MICBIAS",
			"MIC1RP", "Microphone Jack",
			"Headphone Jack", "HPR",
			"Speaker", "SPK";

		cpu_dai: simple-audio-card,cpu {
			sound-dai = <&sai2>;
		};

		codec_dai: simple-audio-card,codec {
			sound-dai = <&tlv320aic3100>;
			clocks = <&audio_blk_ctrl IMX8MP_CLK_AUDIOMIX_SAI2_MCLK1>;
		};
	};

	sound-wwan {
		compatible = "simple-audio-card";
		simple-audio-card,name = "Modem";
		simple-audio-card,format = "dsp_a";
		simple-audio-card,frame-master = <&wwan_dai>;
		simple-audio-card,bitclock-master = <&wwan_dai>;
    simple-audio-card,bitclock-inversion = <&wwan_dai>;

		wwan_cpu_dai: simple-audio-card,cpu {
			sound-dai = <&sai3>;
		};

		wwan_dai: simple-audio-card,codec {
			sound-dai = <&wwan_codec>;
		};
	};

	wwan_codec: sound-wwan-codec {
		compatible = "option,gtm601";
		#sound-dai-cells = <0>;
	};
};

&snvs_rtc {
	status = "disabled";
};

&uart1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart1>;
	assigned-clocks = <&clk IMX8MP_CLK_UART1>;
	assigned-clock-parents = <&clk IMX8MP_SYS_PLL1_80M>;
	uart-has-rtscts;
	status = "okay";

	bluetooth {
		compatible = "qcom,qca9377-bt";
		enable-gpios = GP_BT_ENABLE;
		max-speed = <3000000>;
	};
};

&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	status = "okay";
};

&uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart3>;
	assigned-clocks = <&clk IMX8MP_CLK_UART3>;
	assigned-clock-parents = <&clk IMX8MP_SYS_PLL1_80M>;
	status = "okay";
};

&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart4>;
	assigned-clocks = <&clk IMX8MP_CLK_UART4>;
	assigned-clock-parents = <&clk IMX8MP_SYS_PLL1_80M>;
	status = "okay";
};

&usb3_0 {
	fsl,disable-port-power-control;
	status = "okay";
};

&usb3_1 {
	fsl,disable-port-power-control;
	status = "okay";
};

&usb3_phy0 {
	status = "okay";
};

&usb3_phy1 {
	status = "okay";
};

&usb_dwc3_0 {
	dr_mode = "host";
	status = "okay";
};

&usb_dwc3_1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usb1_vbus>;
	dr_mode = "host";
	status = "okay";
};

&sai2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai2>;
	assigned-clocks = <&clk IMX8MP_CLK_AUDIOMIX_SAI2_MCLK1>;
	assigned-clock-parents = <&clk IMX8MP_AUDIO_PLL2_OUT>;
	assigned-clock-rates = <24576000>;
	fsl,sai-mclk-direction-output;
	#sound-dai-cells = <0>;
	status = "okay";
};

&sai3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_sai3>;
	assigned-clocks = <&clk IMX8MP_CLK_SAI3>;
	assigned-clock-parents = <&clk IMX8MP_AUDIO_PLL1_OUT>;
	assigned-clock-rates = <24576000>;
	fsl,sai-mclk-direction-output;
	#sound-dai-cells = <0>;
	status = "okay";
};

&pwm1 {
	//pinctrl-names = "default";
	//pinctrl-0 = <&pinctrl_pwm1>;
	status = "okay";
};

&pwm2 {
	//pinctrl-names = "default";
	//pinctrl-0 = <&pinctrl_pwm2>;
	status = "okay";
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,ext-reset-output;
	timeout-sec = <7200>;
	status = "okay";
};

&i2c1 {
	clock-frequency = <400000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c1>;
	pinctrl-1 = <&pinctrl_i2c1_1>;
	scl-gpios = GP_I2C1_SCL;
	sda-gpios = GP_I2C1_SDA;
	status = "okay";

	pmic: pca9450@25 {
		reg = <0x25>;
		compatible = "nxp,pca9450c";
		/* PMIC PCA9450 PMIC_nINT GPIO1_IO3 */
		pinctrl-0 = <&pinctrl_pmic>;
		interrupt-parent = <&gpio1>;
		interrupts = <3 IRQ_TYPE_LEVEL_LOW>;

		regulators {
			buck1: BUCK1 {
				regulator-name = "BUCK1";
				regulator-min-microvolt = <600000>;
				regulator-max-microvolt = <2187500>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <3125>;
			};

			buck2: BUCK2 {
				regulator-name = "BUCK2";
				regulator-min-microvolt = <600000>;
				regulator-max-microvolt = <2187500>;
				regulator-limit-microvolt = <1025000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <3125>;
				nxp,dvs-run-voltage = <950000>;
				nxp,dvs-standby-voltage = <850000>;
			};

			buck4: BUCK4 {
				regulator-name = "BUCK4";
				regulator-min-microvolt = <600000>;
				regulator-max-microvolt = <3400000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck5: BUCK5 {
				regulator-name = "BUCK5";
				regulator-min-microvolt = <600000>;
				regulator-max-microvolt = <3400000>;
				regulator-boot-on;
				regulator-always-on;
			};

			buck6: BUCK6 {
				regulator-name = "BUCK6";
				regulator-min-microvolt = <600000>;
				regulator-max-microvolt = <3400000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo1: LDO1 {
				regulator-name = "LDO1";
				regulator-min-microvolt = <1600000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo2: LDO2 {
				regulator-name = "LDO2";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1150000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo3: LDO3 {
				regulator-name = "LDO3";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo4: LDO4 {
				regulator-name = "LDO4";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			ldo5: LDO5 {
				regulator-name = "LDO5";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};
		};
	};
};

&A53_0 {
	cpu-supply = <&buck2>;
};

&A53_1 {
	cpu-supply = <&buck2>;
};

&A53_2 {
	cpu-supply = <&buck2>;
};

&A53_3 {
	cpu-supply = <&buck2>;
};

&i2c2 {
	clock-frequency = <100000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c2>;
	pinctrl-1 = <&pinctrl_i2c2_1>;
	scl-gpios = GP_I2C2_SCL;
	sda-gpios = GP_I2C2_SDA;
	status = "okay";
};

&i2c3 {
	clock-frequency = <100000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c3>;
	pinctrl-1 = <&pinctrl_i2c3_1>;
	scl-gpios = GP_I2C3_SCL;
	sda-gpios = GP_I2C3_SDA;
	status = "okay";

	tlv320aic3100: codec@18 {
		compatible = "ti,tlv320aic3100";
		reg = <0x18>;
		#sound-dai-cells = <0>;
		clocks = <&audio_blk_ctrl IMX8MP_CLK_AUDIOMIX_SAI2_MCLK1>;
		clock-names = "mclk";
		ai31xx-micbias-vg = <2>;

		HPVDD-supply = <&reg_main_3v3>;
		SPRVDD-supply = <&reg_main_5v>;
		SPLVDD-supply = <&reg_main_5v>;
		AVDD-supply = <&reg_main_3v3>;
		IOVDD-supply = <&reg_main_3v3>;
		DVDD-supply = <&reg_main_1v8>;

		status = "okay";
	};

	rtc@68 {
		compatible = "nxp,pcf8523";
		reg = <0x68>;
	};
};

&i2c4 {
	clock-frequency = <100000>;
	pinctrl-names = "default", "gpio";
	pinctrl-0 = <&pinctrl_i2c4>;
	pinctrl-1 = <&pinctrl_i2c4_1>;
	scl-gpios = GP_I2C4_SCL;
	sda-gpios = GP_I2C4_SDA;
	status = "disabled";
};

&mipi_dsi {
	#address-cells = <1>;
	#size-cells = <0>;
	samsung,burst-clock-frequency = <972000000>;
	samsung,esc-clock-frequency = <48000000>;
	status = "okay";

	panel@0 {
		compatible = "jdi,lt070me05000";
		reg = <0>;

		pinctrl = <&pinctrl_panel>;

		// reset is driven by rp2040
		// dcdc en is also driven by rp2040
		// actually backlight enable
		enable-gpios = <&gpio1 7 GPIO_ACTIVE_HIGH>;
		burst-mode;

		port {
			panel_in: endpoint {
				remote-endpoint = <&mipi_dsi_out>;
			};
		};
	};

	ports {
		port@1 {
			reg = <1>;
			mipi_dsi_out: endpoint {
				remote-endpoint = <&panel_in>;
			};
		};
	};
};

&lcdif1 {
	status = "okay";
};

&lcdif3 {
	status = "okay";
};

&hdmi_pvi {
	status = "okay";
};

&hdmi_tx {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_hdmi>;
	status = "okay";
};

&hdmi_tx_phy {
	status = "okay";
};

&ecspi2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_ecspi2>;
	cs-gpios = GP_ECSPI2_CS;
	status = "okay";

	spidev@0 {
		compatible = "mntre,lpc11u24";
		spi-max-frequency = <1000000>;
		reg = <0>;
	};
};

&eqos {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_eqos>;
	phy-mode = "rgmii-id";
	phy-handle = <&ethphy0>;
	status = "okay";

	mdio {
		compatible = "snps,dwmac-mdio";
		#address-cells = <1>;
		#size-cells = <0>;

		// since module 2.0, this is 0. before it was 4.
		ethphy0: ethernet-phy {
			//reg = <0>;
			compatible = "ethernet-phy-ieee802.3-c22";
			at803x,hib-disabled;
			eee-broken-1000t;
			interrupts-extended = GPIRQ_EQOS_PHY;
			//reset-gpios = GP_EQOS_RESET;
			reg-mask = <0x90>;
		};
	};
};

// microsd card
&usdhc1 {
	assigned-clocks = <&clk IMX8MP_CLK_USDHC1_ROOT>;
	assigned-clock-rates = <400000000>;
	//max-frequency = <12000000>;
	bus-width = <4>;
	cd-gpios = GP_USDHC1_CD;
	pinctrl-names = "default"; //, "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc1>, <&pinctrl_usdhc1_gpio>;
	pinctrl-1 = <&pinctrl_usdhc1_100mhz>, <&pinctrl_usdhc1_gpio>;
	pinctrl-2 = <&pinctrl_usdhc1_200mhz>, <&pinctrl_usdhc1_gpio>;
	status = "okay";
	vmmc-supply = <&reg_main_3v3>;
	vqmmc-supply = <&reg_main_3v3>;
};

// sdio (wifi/bt qca9733)
&usdhc2 {
	//assigned-clocks = <&clk IMX8MP_CLK_USDHC2_ROOT>;
	//assigned-clock-rates = <200000000>;
	bus-width = <4>;
	//max-frequency = <100000000>;
	keep-power-in-suspend;
	no-sd-uhs-sdr104;
	non-removable;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc2>;
	pinctrl-1 = <&pinctrl_usdhc2_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc2_200mhz>;
	pm-ignore-notify;
	status = "okay";
	vmmc-supply = <&reg_wlan_vmmc>;
	vqmmc-1-8-v;
	vqmmc-supply = <&reg_main_1v8>;
};

// emmc
&usdhc3 {
	bus-width = <8>; // FIXME was 4
	non-removable;
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc3>;
	pinctrl-1 = <&pinctrl_usdhc3_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc3_200mhz>;
	vmmc-supply = <&reg_main_3v3>;
	vqmmc-1-8-v;
	vqmmc-supply = <&reg_main_1v8>;
	status = "okay";
};

&pcie_phy {
	fsl,refclk-pad-mode = <IMX8_PCIE_REFCLK_PAD_OUTPUT>;
	clocks = <&hsio_blk_ctrl>;
	phy-supply = <&reg_pcie0>;
	clock-names = "ref";
	status = "okay";
	fsl,clkreq-unsupported;
};

&pcie {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pcie>;
	reset-gpio = GP_PCIE_RESET;
	//disable-gpio = GP_PCIE_DISABLE;
	clocks = <&clk IMX8MP_CLK_HSIO_ROOT>,
		 <&clk IMX8MP_CLK_PCIE_ROOT>,
		 <&clk IMX8MP_CLK_HSIO_AXI>;
	clock-names = "pcie", "pcie_aux", "pcie_bus";
	assigned-clocks = <&clk IMX8MP_CLK_PCIE_AUX>;
	assigned-clock-rates = <10000000>;
	assigned-clock-parents = <&clk IMX8MP_SYS_PLL2_50M>;
	vpcie-supply = <&reg_pcie0>;
	vph-supply = <&reg_pcie0>;
	status = "okay";
};
