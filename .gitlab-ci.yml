image: debian:bookworm-slim

variables:
  BASESUITE:
    description: Debian suite to fetch packages from
    value: unstable
  OURSUITE:
    description: Suite name of the repository we create
    value: reform
  REFORM_TOOLS_BRANCH:
    description: The git branch name from which reform-tools should be built
    value: main

stages:
  - setup
  - build
  - reprepro

.setup:
  before_script: |
    set -x
    apt-get update -o Acquire::AllowReleaseInfoChange=true -o quiet::ReleaseInfoChange=true
    apt-get --no-install-recommends -y install devscripts reprepro sbuild debhelper debian-keyring quilt pristine-tar rsync git python3-debian faketime python3-jinja2 python3-dacite uidmap debian-archive-keyring curl kernel-wedge unzip
    if [ "$(dpkg --print-architecture)" != "arm64" ]; then
      if [ ! -e /proc/sys/fs/binfmt_misc/status ]; then
        mount -t binfmt_misc binfmt_misc /proc/sys/fs/binfmt_misc
      fi
      apt-get --no-install-recommends -y install arch-test binfmt-support qemu-user-static
      arch-test arm64
    fi
    adduser --comment build --disabled-password build
    runuser -u build -- sh -c 'echo "\$chroot_mode = \"unshare\";1;" > ~/.sbuildrc'
    runuser -u build -- sh -c 'mkdir -p changes buildlogs ~/.cache/sbuild'
    if [ "$BASESUITE" = "experimental" ]; then
      runuser -u build -- sh -c "ln -s unstable-$(dpkg --print-architecture).tar ~/.cache/sbuild/experimental-$(dpkg --print-architecture).tar"
    fi
    for arch in $(dpkg --print-architecture); do
      # running in a runuser sub-shell to properly expand ~
      runuser -u build -- sh -c "cp chroot-${arch}.tar ~/.cache/sbuild/$([ "$BASESUITE" = "experimental" ] && echo unstable || echo "$BASESUITE")-$arch.tar"
    done
    chown build -R .

setup:
  stage: setup
  artifacts:
    paths:
      - chroot-*.tar
      - repo
      - chdist
  script: |
    set -x
    # Do not allow custom builds in the 'reform' namespace as those can
    # potentially taint the repository at https://mntre.com/reform-debian-repo/
    if [ "$CI_SERVER_HOST" = "source.mnt.re" ] && [ "$CI_PROJECT_NAMESPACE" = "reform" ]; then
      if [ "$BASESUITE" != "unstable" ]; then
        echo "Refusing to run pipeline in the 'reform' namespace with BASESUITE not set to 'unstable'" >&2
        echo "to avoid accidentally tainting the repository at https://mntre.com/reform-debian-repo." >&2
        echo "Consider running custom pipelines in a fork of reform-debian-packages under your own username instead." >&2
        exit 1
      fi
      if [ "$OURSUITE" != "reform" ]; then
        echo "Refusing to run pipeline on the 'reform' namespace with OURSUITE not set to 'reform'" >&2
        echo "to avoid accidentally tainting the repository at https://mntre.com/reform-debian-repo." >&2
        echo "Consider running custom pipelines in a fork of reform-debian-packages under your own username instead." >&2
        exit 1
      fi
    fi
    apt-get update -o Acquire::AllowReleaseInfoChange=true -o quiet::ReleaseInfoChange=true
    apt-get --no-install-recommends -y install reprepro python3 devscripts mmdebstrap uidmap
    if [ "$(dpkg --print-architecture)" != "arm64" ]; then
      if [ ! -e /proc/sys/fs/binfmt_misc/status ]; then
        mount -t binfmt_misc binfmt_misc /proc/sys/fs/binfmt_misc
      fi
      apt-get --no-install-recommends -y install arch-test binfmt-support qemu-user-static
      arch-test arm64
    fi
    adduser --comment build --disabled-password build
    chown build -R .
    rm -rvf changes buildlogs chdist repo chroot-*.tar
    runuser -u build -- mmdebstrap --architecture=$(dpkg --print-architecture) --variant=buildd --mode=unshare \
      --chrooted-customize-hook='rm /usr/bin/ischroot && ln -s /bin/true /usr/bin/ischroot' ${IFS# DebianBug:1056385} \
      "$([ "$BASESUITE" = "experimental" ] && echo unstable || echo "$BASESUITE")" chroot-$(dpkg --print-architecture).tar
    #runuser -u build -- mmdebstrap --architecture=arm64 --variant=buildd --mode=unshare \
    #  --chrooted-customize-hook='rm /usr/bin/ischroot && ln -s /bin/true /usr/bin/ischroot' ${IFS# DebianBug:1056385} \
    #  "$([ "$BASESUITE" = "experimental" ] && echo unstable || echo "$BASESUITE")" chroot-arm64.tar
    runuser -u build -- sh -x ./setup.sh

build_patched:
  stage: build
  #when: manual
  extends: .setup
  dependencies:
    - setup
  artifacts:
    when: always
    paths:
      - output.log
      - changes
      - buildlogs
  script: |
    ## if we are on gitlab CI and if the current branch is NOT main, then
    ## fill the repo with cached results from the last successful run in main
    #for p in patches/*; do
    #  p=${p#patches/}
    #  [ -e "patches/$p" ] || continue
    #  [ -x "patches/$p" ] || continue
    #  if [ -n "${CI_COMMIT_REF_NAME+x}" ] && [ "${CI_COMMIT_REF_NAME:-}" != main ]; then
    #    for variant in cross native; do
    #      if [ "$(curl --silent --output /dev/null --write-out "%{http_code}" --location "$CI_PROJECT_URL/-/jobs/artifacts/main/raw/changes/${p}_${variant}.changes?job=build_patched")" != "200" ]; then
    #        echo "${p}_${variant}.changes is not available" >&2
    #        continue
    #      fi
    #      curl --silent --location "$CI_PROJECT_URL/-/jobs/artifacts/main/raw/changes/${p}_${variant}.changes?job=build_patched" > "changes/${p}_${variant}.changes"
    #      env --chdir=changes dcmd "${p}_${variant}.changes" | while read f; do
    #        echo "downloading $f" >&2
    #        curl --silent --location "$CI_PROJECT_URL/-/jobs/artifacts/main/raw/changes/${f}?job=build_patched" > "changes/${f}"
    #      done
    #      echo "including ${p}_${variant}.changes" >&2
    #      runuser -u build -- reprepro --basedir ./repo include "reform" "changes/${p}_${variant}.changes"
    #    done
    #  fi
    #done
    if [ "$BASESUITE" = "experimental" ]; then
      BASESUITE=unstable
    fi
    runuser -u build -- sh -x ./build_patched.sh 2>&1 | ./filter-output

build_custom:
  stage: build
  #when: manual
  extends: .setup
  dependencies:
    - setup
  artifacts:
    when: always
    paths:
      - output.log
      - changes
      - buildlogs
      - fonts-reform-iosevka-term_2.3.0-1_all.deb
  script: |
    runuser -u build -- sh -x ./build_custom.sh 2>&1 | ./filter-output
  after_script: |
    echo "BASESUITE: $BASESUITE"
    echo "OURSUITE: $OURSUITE"
    echo "REFORM_TOOLS_BRANCH: $REFORM_TOOLS_BRANCH"

build_linux:
  stage: build
  #when: manual
  extends: .setup
  dependencies:
    - setup
  artifacts:
    when: always
    paths:
      - output.log
      - changes
      - linux/linux_*_arm64-*.build
  script: |
    set -x
    runuser -u build -- sh -xc '. ./common.sh; cd linux; . ./build.sh' 2>&1 | ./filter-output
    if ! dpkg-deb -c changes/linux-image-*-mnt-reform-arm64_*_arm64.deb | grep 'kernel/drivers/gpu/drm/imx\(/cdns\)\?/cdns_mhdp_imx.ko' \
      && ! dpkg-deb -c changes/linux-image-*-mnt-reform-arm64_*_arm64.deb | grep 'kernel/drivers/gpu/drm/bridge/cadence/cdns-mhdp8546.ko'; then
      echo "cdns missing" >&2
      exit 1
    fi
    for dtb in amlogic/meson-g12b-bananapi-cm4-mnt-pocket-reform.dtb freescale/imx8mp-mnt-pocket-reform.dtb freescale/imx8mq-mnt-reform2.dtb freescale/imx8mq-mnt-reform2-hdmi.dtb amlogic/meson-g12b-bananapi-cm4-mnt-reform2.dtb freescale/imx8mp-mnt-reform2.dtb freescale/fsl-ls1028a-mnt-reform2.dtb rockchip/rk3588-mnt-reform2.dtb; do
      if ! dpkg-deb -c changes/linux-image-*-mnt-reform-arm64_*_arm64.deb | grep "/${dtb}\$"; then
        echo "$dtb missing" >&2
        exit 1
      fi
    done
    for f in changes/linux-image-arm64_*_arm64.deb changes/linux-headers-arm64_*_arm64.deb; do
      if [ ! -e "$f" ]; then
        echo "$f does not exist" >&2
        exit 1
      fi
    done

  after_script: |
    echo "BASESUITE: $BASESUITE"
    echo "OURSUITE: $OURSUITE"
    echo "REFORM_TOOLS_BRANCH: $REFORM_TOOLS_BRANCH"

build_qcacld:
  stage: build
  needs:
    - job: build_linux
      artifacts: true
  script: |
    set -x
    apt-get update -o Acquire::AllowReleaseInfoChange=true -o quiet::ReleaseInfoChange=true
    apt-get --yes install --no-install-recommends gcc-aarch64-linux-gnu build-essential bison flex device-tree-compiler git ca-certificates xz-utils libssl-dev bc

    # debug
    echo "build_qcacld debug:"
    echo "$(pwd):"
    echo "ls -lh"
    echo "ls -lh changes"

    # find the linux debs
    cd changes
    LINUX_IMG_DEB=$(ls *.deb | grep -e '^linux-image-[0-9]' | grep -v '\-rt-' | head -n1)
    LINUX_SRC_DEB=$(ls *.deb | grep -e '^linux-source-[0-9]' | head -n1)
    LINUX_CFG_DEB=$(ls *.deb | grep -e '^linux-config-[0-9]' | head -n1)
    cd ..

    echo "build_qcacld debug vars:"
    echo "img: ${LINUX_IMG_DEB}"
    echo "src: ${LINUX_SRC_DEB}"
    echo "cfg: ${LINUX_CFG_DEB}"

    KERNELPACKAGENAME=$(echo $LINUX_IMG_DEB | cut -d '_' -f 1)
    KERNELVERSION=$(echo $LINUX_IMG_DEB | cut -d '_' -f 2)
    KERNELRELEASE=${KERNELPACKAGENAME/linux-image-/}

    # clone driver
    git clone --branch binary-deb https://source.mnt.re/reform/qcacld2.git
    cd qcacld2

    # extract kernel source
    dpkg -x "../changes/${LINUX_SRC_DEB}" ./
    tar xfJ usr/src/linux-source-*.tar.xz

    # extract kernel config
    dpkg -x "../changes/${LINUX_CFG_DEB}" ./
    unxz usr/src/linux-config-*/config.arm64_none_*arm64.xz

    # install kernel config
    KERNSRC=$(echo linux-source-*)
    mv usr/src/linux-config-*/config.arm64_none_*arm64 "${KERNSRC}/.config"

    # prepare kernel tree
    cd $KERNSRC
    make -j $(nproc) ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- modules_prepare
    cd ..

    # build driver
    KBUILD_MODPOST_WARN=1 ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- MODNAME=qcacld2 KERNEL_SRC=${KERNSRC} make

    # build debian package
    QCAPACKAGEVERSION=$(date -u +"%Y%m%dT%H%M%SZ")
    mkdir -p debian/opt/reform-qcacld2
    mv qcacld2.ko "debian/opt/reform-qcacld2/qcacld2-${KERNELVERSION}.ko"
    sed -i "s/KERNELRELEASE/$KERNELRELEASE/g" debian/DEBIAN/control
    sed -i "s/QCAPACKAGEVERSION/$QCAPACKAGEVERSION/g" debian/DEBIAN/control
    sed -i "s/KERNELPACKAGENAME/$KERNELPACKAGENAME/g" debian/DEBIAN/control
    ## permissions are world writable by default in CI, fix that
    chmod -R og-w debian
    ls -lR debian
    dpkg --build debian "reform-qcacld2-${KERNELRELEASE}_${QCAPACKAGEVERSION}_arm64.deb"

    # build debian meta-package that tracks the latest version of the
    # actual driver package so it is auto-upgraded together with kernel upgrades
    # and that also includes a NeverAutoRemove snippet for /etc/apt/apt.conf.d/
    # this package also contains the shared firmware for the drivers.
    sed -i "s/KERNELRELEASE/$KERNELRELEASE/g" debian-meta/DEBIAN/control
    sed -i "s/QCAPACKAGEVERSION/$QCAPACKAGEVERSION/g" debian-meta/DEBIAN/control
    ## permissions are world writable by default in CI, fix that
    chmod -R og-w debian-meta
    ls -lR debian-meta
    dpkg --build debian-meta "reform-qcacld2_${QCAPACKAGEVERSION}_arm64.deb"
    mv *.deb ..
    cd ..

  artifacts:
    when: always
    paths:
      - "reform-qcacld2*.deb"

reprepro:
  stage: reprepro
  dependencies:
    - setup
    - build_patched
    - build_custom
    - build_linux
    - build_qcacld
  artifacts:
    when: always
    paths:
      - repo
      - variables.sh
  script: |
    set -x
    apt-get update -o Acquire::AllowReleaseInfoChange=true -o quiet::ReleaseInfoChange=true
    apt-get --no-install-recommends -y install reprepro python3 devscripts
    adduser --comment build --disabled-password build
    runuser -u build -- sh -x ./reprepro.sh
    {
      echo "CI_PROJECT_NAMESPACE=\"$CI_PROJECT_NAMESPACE\"";
      echo "CI_COMMIT_BRANCH=\"$CI_COMMIT_BRANCH\"";
      echo "CI_COMMIT_REF_NAME=\"$CI_COMMIT_REF_NAME\"";
      echo "BASESUITE=\"$BASESUITE\"";
      echo "OURSUITE=\"$OURSUITE\"";
      echo "REFORM_TOOLS_BRANCH=\"$REFORM_TOOLS_BRANCH\"";
    } | tee variables.sh
